# Conectarse al servidor SQL
```shell
mysql -h 127.0.0.1 -u root -p --protocol=tcp
```

# Ejecutar Comandos de SQL

## Crear Base de datos
```sql
create database integrador_cac;
```

## Moverse a la base de datos creada
```sql
use integrador_cac;
```

## Crear tabla de usuarios
```sql
create table oradores (
    id_orador int auto_increment primary key,
    nombre varchar(40) not null,
    apellido varchar(40) not null,
    mail varchar(100) not null,
    tema varchar(50) not null,
    fecha_alta datetime default current_timestamp,
    unique (mail)
);
```

## Insertar 5 registros
```sql
insert into oradores (nombre,apellido,mail,tema) values 
("nombre1","apellido1", "nombreapellido1@mail.com","tema1"),
("nombre2","apellido2", "nombreapellido2@mail.com","tema2"),
("nombre3","apellido3", "nombreapellido3@mail.com","tema3"),
("nombre4","apellido4", "nombreapellido4@mail.com","tema4"),
("nombre5","apellido5", "nombreapellido5@mail.com","tema5"),
("nombre6","apellido6", "nombreapellido6@mail.com","tema6"),
("nombre7","apellido7", "nombreapellido7@mail.com","tema7"),
("nombre8","apellido8", "nombreapellido8@mail.com","tema8"),
("nombre9","apellido9", "nombreapellido9@mail.com","tema9"),
("nombre10","apellido10", "nombreapellido10@mail.com","tema10");
```

## Ver usuarios
```sql
select * from oradores;
```

## Mostrar descripción de tabla creada
```sql
desc oradores;
```

# Hacer backup de base de datos
```shell
mysqldump -h 127.0.0.1 -u root -p --protocol=tcp integrador_cac > backup-integrador_cac.sql
```

# Evidencias

## Conectarse al servidor SQL
![conectar-servidor](images/conectar-servidor.png)

## Crear base de datos
![crear-bd](images/crear-bd.png)

## Moverse a base de datos
![usar-bd](images/usar-bd.png)

## Crear tabla oradores
![crear-tabla](images/crear-tabla.png)

## Insertar 10 registros
![insertar-registros](images/insertar-registros.png)

## Ver oradores
![ver-oradores](images/ver-oradores.png)

## Hacer backup de base de datos
![backup-comando](images/backup-comando.png)
![backup-archivo](images/backup-archivo.png)